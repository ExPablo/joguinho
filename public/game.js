'use strict';

let app = angular.module("GameApp" ,[]);

app.controller('Board', ($scope) => {
	let mapSize = 20;
	$scope.player = {
		lvl: 1,
		name: 'John das Neves',
		x: 1,
		y: 1,
		score:0,
		key:0,
		Mlife:20,
		life:20,
		atk:10,
		def:0,
		sword:0,
		shield:0,
		feno:0,
		quest1:0,
		move:1
	};

	$scope.gameOver = false;

	$scope.monsterdefault = {
					name:'none',
					atk: 0 ,
					life: 0,
					Mlife:0,
					def:0,
					exp: 0,
					drop:0,
					battle:0
				};



	$scope.npc1 = {
		name:'mario',
		y:1,
		x:5,
		ativo:0
	};

	$scope.monster = $scope.monsterdefault;

	$scope.logg = [{desc:'', color:0}];

	$scope.itens = [
		//0
		{name:'Nada'},
		// 1
		{name:'Small Healt Potion', life:10},
		// 2
		{name:'Adaga',type:'0' , atk:5},
		// 3
		{name:'Short Sword',type:'0' , atk:16},
		// 4
		{name:'Bone Blade',type:'0' , atk:18},
		// 5
		{name:'Axe',type:'0' , atk:22},
		// 6
		{name:'War Hammer',type:'0' , atk:30},
		// 7
		{name:'Minotaur Axe',type:'0' , atk:26},
		// 8
		{name:'Broquel',type:'1' , def:3},
		// 9
		{name:'wooden Shield',type:'1' , def:7},
		// 10
		{name:'Bone Shield',type:'1' , def:10},
		// 11
		{name:'Frost Shield',type:'1' , def:15},
		// 12
		{name:'Tower Shield',type:'1' , def:20}

	];

	function generateMap(size) {
		let map = [];
		for(let r=0; r < size; r++)	{
			let row = [];
			for(let c=0; c < size; c++)	{

				row.push({x: c, y: r, tid: 1 , id: 1 , blood:0 , sqm:1});
			}
			map.push(row);
		}

// TID
// 1 = grama
// 2 = pedra
// 3 = arvore
// 4 = bau
// 5 = chave
// 6 = mosnter
// 7 = bag
// 8 = blue chest
// 9 = firecamp
// 10 = muro
// 11 = cantomuro
// 12 = muro Horizontal
// 13 = gate fechado
// 14 = door
// 15 = gate aberto
// 16 = npc
// 17 = feno

// GID
// 1 = bear
// 2 = sword fraca
// 3 = shield
// 4 = sword forte

//casinha 
		 let d = 10;
		for (let a = 0; d >= a; a++) {
			for(let b = 0; d >= b;b++){
				map[a][b].sqm = 4;
			}
		}

		for (let a = 1; d > a; a++) {
			map[a][0].tid = 10;
		}

		for (let a = 1; d > a; a++) {
			map[0][a].tid = 12;
		}

			map[0][0].tid = 11;
			map[1][1].sqm = 5;
			map[0][4].tid = 11;
			map[1][5].tid = 16;
			map[1][4].tid = 10;
			map[2][4].tid = 10;
			map[3][0].tid = 11;
			map[3][1].tid = 12;
			map[3][2].tid = 14;
			map[3][3].tid = 12;
			map[3][4].tid = 11;
			map[3][5].tid = 12;
			map[3][6].tid = 14;
			map[3][7].tid = 12;
			map[3][8].tid = 12;
			map[3][9].tid = 12;
			map[7][1].tid = 17;
			map[8][1].tid = 17;
			map[9][1].tid = 17;
			map[10][0].tid = 11;
			map[0][10].tid = 11;
			map[10][10].tid = 11;

		for (let a = 1; d > a; a++) {
			map[a][10].tid = 10;
		}

	
		map[3][10].tid = 11;
		map[10][9].tid = 12;
		map[10][8].tid = 12;
		map[10][7].tid = 12;
		map[10][6].tid = 12;
		map[10][5].tid = 13;
		map[10][4].tid = 12;
		map[10][3].tid = 12;
		map[10][2].tid = 12;
		map[10][1].tid = 12;


//cerco da serpent
		map[5][5].sqm = 2;
		map[6][5].sqm = 2;
		map[7][5].sqm = 2;
		map[5][6].sqm = 2;
		map[6][6].sqm = 2;
		map[6][6].tid = 6;
		map[6][6].id = 1;
		map[7][6].sqm = 2;
		map[5][7].sqm = 2;
		map[6][7].sqm = 2;
		map[7][7].sqm = 2;

//cerco do bau master
		
		map[0][17].tid = 3;
		map[0][18].tid = 8;
		map[0][19].tid = 3;
		map[1][17].tid = 3;
		map[1][18].tid = 3;
		map[1][19].tid = 3;

//cave dos hero
		
		map[0][12].tid = 2;
		map[0][13].tid = 2;
		map[0][14].tid = 2;
		map[1][12].tid = 2;
		map[1][13].tid = 6;
		map[1][13].id = 2;
		map[1][14].tid = 2;
		map[2][12].tid = 2;
		map[2][14].tid = 2;
		map[2][11].tid = 2;
		map[2][10].tid = 2;
		map[2][15].tid = 2;
		map[2][16].tid = 2;
		map[3][10].tid = 2;
		map[3][11].tid = 6;
		map[3][11].id = 3;
		map[3][13].tid = 6;
		map[3][13].id = 4;
		map[3][15].tid = 6;
		map[3][15].id = 5;
		map[3][16].tid = 2;
		map[4][10].tid = 2;
		map[4][12].tid = 2;
		map[4][13].tid = 2;
		map[4][14].tid = 2;
		map[4][16].tid = 2;

//goblin firecamp
		
		map[3][18].tid = 9;
		map[3][17].tid = 6;
		map[3][17].id = 6;
		map[3][19].tid = 6;
		map[3][19].id = 7;

//enf vilage
		map[10][14].tid = 3;
		map[10][15].tid = 3;
		map[10][16].tid = 3;
		map[10][17].tid = 3;
		map[10][18].tid = 3;
		map[10][19].tid = 3;
		map[11][13].tid = 3;
		map[11][14].tid = 3;
		map[11][15].tid = 3;
		map[11][16].tid = 3;
		map[11][17].tid = 3;
		map[11][18].tid = 3;
		map[11][19].tid = 3;
		map[12][12].tid = 3;
		map[12][13].tid = 3;
		map[13][12].tid = 3;
		map[13][13].tid = 3;
		map[13][15].tid = 6;
		map[13][15].id = 8;
		map[13][17].tid = 6;
		map[13][17].id = 9;
		map[13][19].tid = 6;
		map[13][19].id = 10;
		map[14][13].tid = 3;
		map[16][13].tid = 3;
		map[16][13].tid = 3;
		map[17][12].tid = 3;
		map[17][13].tid = 3;
		map[17][15].tid = 6;
		map[17][15].id = 11;
		map[17][17].tid = 6;
		map[17][17].id = 12;
		map[17][19].tid = 6;
		map[17][19].id = 13;
		map[18][11].tid = 3;
		map[18][12].tid = 3;
		map[18][13].tid = 3;
		map[19][10].tid = 3;
		map[19][11].tid = 3;
		map[19][12].tid = 3;
		map[19][13].tid = 3;
		return map;
	}

	function movenpc(y,x){
		setInterval(function() {
			console.log(y,x);
		}, 1000);
	}

	function haveKey(){
		if($scope.player.key > 0){
			$scope.player.key = $scope.player.key - 1;
			return true;
		}else{
			return false;
		}
	}

	function geraXp(xp){
		$scope.player.score = $scope.player.score + xp;
		if($scope.player.score >= 10 && $scope.player.lvl == 1){
			levelUp();
		}

		if($scope.player.score >= 40 && $scope.player.lvl == 2){
			levelUp();
		}

		if($scope.player.score >= 110 && $scope.player.lvl == 3){
			levelUp();
		}

		if($scope.player.score >= 260 && $scope.player.lvl == 4){
			levelUp();
		}

		if($scope.player.score >= 550 && $scope.player.lvl == 5){
			levelUp();
		}

		if($scope.player.score >= 1060 && $scope.player.lvl == 6){
			levelUp();
		}

		if($scope.player.score >= 1890 && $scope.player.lvl == 7){
			levelUp();
		}

		if($scope.player.score >= 3160 && $scope.player.lvl == 8){
			levelUp();
		}

		if($scope.player.score >= 5010 && $scope.player.lvl == 9){
			levelUp();
		}

		if($scope.player.score >= 7600 && $scope.player.lvl == 10){
			levelUp();
		}

		if($scope.player.score >= 11110 && $scope.player.lvl == 11){
			levelUp();
		}

		if($scope.player.score >= 15740 && $scope.player.lvl == 12){
			levelUp();
		}

		if($scope.player.score >= 21710 && $scope.player.lvl == 13){
			levelUp();
		}

		if($scope.player.score >= 29260 && $scope.player.lvl == 14){
			levelUp();
		}

		if($scope.player.score >= 38650 && $scope.player.lvl == 15){
			levelUp();
		}

		if($scope.player.score >= 50160 && $scope.player.lvl == 16){
			levelUp();
		}

		if($scope.player.score >= 64090 && $scope.player.lvl == 17){
			levelUp();
		}

		if($scope.player.score >= 80760 && $scope.player.lvl == 18){
			levelUp();
		}

		if($scope.player.score >= 100810 && $scope.player.lvl == 19){
			levelUp();
		}
	}

	function levelUp(){
		$scope.player.lvl = $scope.player.lvl + 1;
		$scope.player.Mlife = $scope.player.Mlife + 10;
		$scope.player.life = $scope.player.Mlife;
		$scope.player.atk = $scope.player.atk + 5;
		$scope.player.def = $scope.player.def + 5;
		let desc = 'You Advenced in level.';
		addLog(desc	, 2);
	}

	function makeMonster(id){
		switch(id) {
			case 1:
				$scope.monster = {
					name:'Galinha',
					atk: 0 ,
					life: 10,
					Mlife:10,
					def:0,
					exp:1,
					drop:[0,1,2,3,4,5,6,7,8,9,10,11,12],
					battle:1
				};
				break;
			case 2:
				$scope.monster = {
					name:'Inseto',
					atk: 15 ,
					life: 30,
					Mlife:30,
					def:0,
					exp: 1,
					drop:0,
					battle:1
				};
				break;
			case 3:
				$scope.monster = {
					name:'Aranhas',
					atk: 15 ,
					life: 79,
					Mlife:79,
					def:5,
					exp: 6,
					drop:0,
					battle:1
				};
				break;
			case 4:
				$scope.monster = {
					name:'Lobo',
					atk: 20 ,
					life: 92,
					Mlife:92,
					def:10,
					exp: 15,
					drop:0,
					battle:1
				};
				break;
			case 5:
				$scope.monster = {
					name:'Urso',
					atk: 30 ,
					life: 214,
					Mlife:214,
					def:20,
					exp: 37,
					drop:0,
					battle:1
				};
				break;
			case 6:
				$scope.monster = {
					name:'Fantasma',
					atk: 35 ,
					life: 214,
					Mlife:214,
					def:20,
					exp: 37,
					drop:0,
					battle:1
				};
				break;
			case 7:
				$scope.monster = {
					name:'Esqueleto',
					atk: 50 ,
					life: 255,
					Mlife:255,
					def:20,
					exp: 50,
					drop:0,
					battle:1
				};
				break;
			case 8:
				$scope.monster = {
					name:'Orc',
					atk: 40 ,
					life: 300,
					Mlife:300,
					def:10,
					exp: 40,
					drop:0,
					battle:1
				};
				break;
			case 9:
				$scope.monster = {
					name:'Orc Guerreiro',
					atk: 60 ,
					life: 496,
					Mlife:496,
					def:20,
					exp: 75,
					drop:0,
					battle:1
				};
				break;
			case 10:
				$scope.monster = {
					name:'Zombie',
					atk: 80 ,
					life: 387,
					Mlife:387,
					def:25,
					exp: 94,
					drop:0,
					battle:1
				};
				break;
			case 11:
				$scope.monster = {
					name:'Yeti',
					atk: 100 ,
					life: 387,
					Mlife:387,
					def:30,
					exp: 106,
					drop:0,
					battle:1
				};
				break;
			case 12:
				$scope.monster = {
					name:'Urso Polar',
					atk: 120 ,
					life: 380,
					Mlife:380,
					def:25,
					exp: 114,
					drop:0,
					battle:1
				};
				break;
			case 13:
				$scope.monster = {
					name:'Minotauro',
					atk: 130 ,
					life: 570,
					Mlife:570,
					def:50,
					exp: 134,
					drop:0,
					battle:1
				};
				break;
		}
	}

	function monsterAtk(x,y){
		let px = $scope.player.x;
		let py = $scope.player.y;
		$scope.rows[py][px].blood = 1;
		let dano = $scope.monster.atk - $scope.player.def;
		if(dano < 1){
			dano = 1;
		}
		$scope.player.life = $scope.player.life - dano;
		let d = $scope.monster.name+ ' causou ' +dano+ ' de dano a você';
		addLog(d,0);
		console.log($scope.monster.name+ ' causou ' +dano+ ' de dano a você');
		combat(x,y);
	}

	function combat(x , y){
		let player = $scope.player;
		let	monster = $scope.monster;
		if(player.life > 0 && monster.life > 0){
			let dano = player.atk - monster.def;
			if(dano < 1){
				dano = 1;
			}
			monster.life = monster.life - dano;
			let d = 'Você causou ' +dano+ ' de dano ao ' +$scope.monster.name;
			addLog(d,0);
			console.log('Você causou ' +dano+ ' de dano ao ' +$scope.monster.name);
			$scope.rows[y][x].blood	= 1;
			if(monster.life > 0){
				monsterAtk(x,y);
			}else{
				return true;
			}
		}

		if(player.life > 0 && monster.life < 1){
			return true;
		}

		if(player.life < 1){
			return false;
		}


		console.log();
	}

	function addLog(d,c){
		let desc = {desc:d,color:c}; 
		$scope.logg.push(desc);
	}

	function wallkeable(x , y){
		let idx = $scope.rows[y][x].tid;
		let desc;
		switch(idx) {
		    case 1:
		        return true;
		        break;
		    case 5:
		    	$scope.rows[y][x].tid = 1;
					$scope.player.key = $scope.player.key + 1;
					desc = 'You Have found a Key';
		    		addLog(desc	, 1);
					return true;
		    	break;
		    case 6:
		    	if($scope.monster.battle == 1){
			    	let  status = combat(x,y);
			    	if(status){
			    		$scope.rows[y][x].tid = 7;
			    		$scope.rows[y][x].id = $scope.monster.drop;
			    		geraXp($scope.monster.exp);
			    		$scope.monster = $scope.monsterdefault;
			    	}else{
			    		desc = 'You Loose';
				    	addLog(desc	, 1);
			    		$scope.gameOver = true;
			    	}

		    	}else{
		    		makeMonster($scope.rows[y][x].id);
		    	}

		    	break;
		    case 7:
		    	pegaLoot($scope.rows[y][x].id);
		    	$scope.rows[y][x].tid = 1;
		    	return	true;

		    	break;
		    case 4:
		        if(haveKey()){
		        	$scope.rows[y][x].tid = 1;
		        	desc = 'You Have opened a Chest';
			    	addLog(desc	, 1);
					geraXp(100);
		        	return true;
		        }else{
		        	desc = 'You Have no key';
			    	addLog(desc	, 1);
		        	return false;
		        }
		        break;
		    case 13:
		    	if($scope.player.sword > 0){
		    		$scope.rows[y][x].tid = 15;
		    		return true;
		    	}else{
		    		desc = 'Melhor eu não sair desarmado!';
			    	addLog(desc	, 1);
		    	}
		    	break;
		    case 14:
		    	return true;
		    	break;
		    case 15:
		    	return true;
		    	break;
		    case 16:
		    	quest(1);
		    break;
		    case 17:
		    	$scope.player.feno = $scope.player.feno + 1;
		    	$scope.rows[y][x].tid = 1;
		    	desc = 'Você achou um feno.(que bela bosta).';
		    	addLog(desc,1);
		    	return true;
		    default:
		        return false;
		    break;
		}
	}

	function quest(x){
		let	desc;
		switch (x){
			case 1:
				if($scope.player.quest1 == 0){
					if($scope.npc1.ativo == 0 || $scope.player.feno < 1){
		    		desc = 'Ola aventureiro, que acha de ganhar uma espadinha macanuda?, me aranja 3 trequinho maroto que eu te lanço uma na parceria.';
			    	addLog(desc	, 1);
			    	$scope.npc1.ativo = 1;
			    	}else if($scope.npc1.ativo == 1){
			    		if($scope.player.feno > 0 && $scope.player.feno < 3){
				    		desc = 'Ta faltando malandrinho....';
				    		addLog(desc,1);
				    	}else if($scope.player.feno >= 3){
				    		desc = 'Valeu Meu velho! ta na mão teu premio!';
				    		addLog(desc,1);
				    		$scope.player.sword = 5;
				    		desc = 'Você ganhou uma adaga!';
				    		addLog(desc,1);
				    		$scope.player.feno = 0;
				    		$scope.player.quest1 = 1;
				    	}
				    }
				}else if($scope.player.quest1 == 1){
					desc = 'Não tenho mais quests pra vc mano veio.';
				    		addLog(desc,1);
				}
		    break;
		}
	}

	function pegaLoot(x){
		let p = Math.floor(Math.random() * x.length -1) + 1;
		let drop = $scope.itens[x[p]];
		if(drop.type == 0){
			if($scope.player.sword < drop.atk){
				$scope.player.sword = drop.atk;
			}
		}else if(drop.type == 1){
			if($scope.player.shield < drop.def){
				$scope.player.shield = drop.def;
			}
		}else if(drop.type == 2){
			console.log(drop);
		}else{
			console.log(drop);
		}
	 	
	 }

	$scope.movement = function(event) {
		let key = event.keyCode;
		console.log(key);
		if(key == 115){
			if($scope.player.y+1 < mapSize) {
				if(wallkeable($scope.player.x , $scope.player.y + 1) == true) {
					$scope.player.y++;
					$scope.monster = $scope.monsterdefault;
				}
			}
		}

		if(key == 97){
			if($scope.player.x > 0) {
				if(wallkeable($scope.player.x - 1 , $scope.player.y) == true) {
					$scope.player.x--;
					$scope.monster = $scope.monsterdefault;
					$scope.player.move = 0;
				}
			}
		}

		if(key == 100){
			if($scope.player.x+1 < mapSize) {
				if(wallkeable($scope.player.x + 1 , $scope.player.y) == true) {
					$scope.player.x++;
					$scope.monster = $scope.monsterdefault;
					$scope.player.move = 1;
				}
			}
		}

		if(key == 119){
			if($scope.player.y > 0) {
				if(wallkeable($scope.player.x , $scope.player.y - 1) == true) {
					$scope.player.y--;
					$scope.monster = $scope.monsterdefault;
				}
			}
		}
		if(key == 13){

		}
	}
	$scope.rows = generateMap(mapSize);	
});
