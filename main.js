var express = require('express');
var app = express();

app.use(express.static('public'));

let contador = 0;

app.get('/cont', function (req, res) {
  res.send('Contador atual: '+contador);
});

app.get('/plus', function (req, res) {
	contador++;
  res.send('somou!');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});